<?php
    $base_url = 'http://127.0.0.1:8989/jabberdog';

    //date in mm/dd/yyyy format; or it can be in other formats as well
    $birthDate = "12/21/1987";
    //explode the date to get month, day and year
    $birthDate = explode("/", $birthDate);
    //get age from date or birthdate
    $age = (date("md", date("U", mktime(0, 0, 0, $birthDate[0], $birthDate[1], $birthDate[2]))) > date("md")
        ? ((date("Y") - $birthDate[2]) - 1)
        : (date("Y") - $birthDate[2]));
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Jabberdog Web Design | Freelance Web Designer, Liam Fathers</title>
    <meta name="description" content="Liam Fathers (aka Jabberdog) is a web designer from Nuneaton, in the UK">
    <meta name="keywords" content="Web designer, web developer, graphics design, responsive web design, jabberdog"
    <meta name="author" content="Liam Fathers">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="<?php echo $base_url; ?>/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo $base_url; ?>/css/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo $base_url; ?>/css/app.css"/>
    <!-- Fonts -->
    <link rel='stylesheet' type='text/css' href='//fonts.googleapis.com/css?family=Amatic+SC:400,700'>
    <link rel='stylesheet' type='text/css' href='//fonts.googleapis.com/css?family=PT+Sans:400,700'>
    <link rel="icon" type="image/png" href="<?php echo $base_url; ?>/img/favicon.png">
</head>
<body>

