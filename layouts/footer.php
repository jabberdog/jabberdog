<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <a href="#top" class="logo">Jabberdog Web Design</a>
            </div>
            <div class="col-md-6 col-sm-6 text-right">
                <ul>
                    <li><a href="mailto:liam@jabberdog.co.uk" class="fa fa-envelope" alt="Email" title="Email Me"></a></li>
                    <li><a href="skype:jabberdog?add" class="fa fa-skype" alt="Skype Me" title="Add Me on Skype"></a></li>
                    <li><a href="https://www.facebook.com/liam.fathers" class="fa fa-facebook-official" target="_blank" rel="nofollow" alt="Facebook" title="Facebook"></a></li>
                    <li><a href="https://twitter.com/HokutoPain" class="fa fa-twitter" target="_blank" rel="nofollow" alt="Twitter" title="Twitter"></a></li>
                    <li><a href="https://github.com/jabberdog" class="fa fa-github" target="_blank" rel="nofollow" alt="GitHub" title="Fork Me on GitHub"></a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>
<!-- Scripts -->
<script type="text/javascript" src="<?php echo $base_url; ?>/js/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo $base_url; ?>/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo $base_url; ?>/js/jquery.stellar.js"></script>
<script type="text/javascript">
    $.stellar();
</script>
<script>
    $('#myModal').on('shown.bs.modal', function () {
        $('#myInput').focus()
    })
</script>
<script type="text/javascript">
    $(document).ready(function(){
        $('a[href^="#"]').on('click',function (e) {
            e.preventDefault();

            var target = this.hash;
            var $target = $(target);

            $('html, body').stop().animate({
                'scrollTop': $target.offset().top
            }, 900, 'swing');
        });
    });
</script>
</body>
</html>