<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h1 class="modal-title" id="myModalLabel">Hire Me</h1>
            </div>
            <div class="modal-body">
                <div class="popup">
                    <img src="<?php echo $base_url; ?>/img/high_five.jpg" alt="High Five" class="img-circle img-responsive"/>
                    <h2>Thank You!</h2>
                    <p><em>First of all, let me just start by saying thank you for deciding to hire me.</em></p>
                    <p>Please submit any questions or queries via any of the links below. For queries sent from outside of the UK please allow a little more time for response because of the timezone difference.</p>
                    <ul>
                        <li><a href="mailto:liam@jabberdog.co.uk" class="fa fa-envelope"></a><br>Email</li>
                        <li><a href="skype:jabberdog?add" class="fa fa-skype"></a><br>Skype</li>
                        <li><a href="https://www.facebook.com/liam.fathers" class="fa fa-facebook-official" target="_blank" rel="nofollow"></a><br>Facebook</li>
                        <li><a href="https://twitter.com/HokutoPain" class="fa fa-twitter" target="_blank" rel="nofollow"></a><br>Twitter</li>
                        <li><a href="https://github.com/jabberdog" class="fa fa-github" target="_blank" rel="nofollow"></a><br>GitHub</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
