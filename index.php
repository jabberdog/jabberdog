<?php require_once('./layouts/header.php'); ?>
<section id="top">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-4">
                <a href="#" class="logo">Jabberdog Web Design</a>
            </div>
            <div class="col-md-8 col-sm-8 text-right">
                <a href="#" class="btn btn-default btn-lg" data-toggle="modal" data-target="#myModal">Hire Me</a>
            </div>
        </div>
    </div>
</section>
<section id="cta">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <img src="<?php echo $base_url; ?>/img/home-banner.png" class="img-responsive"/>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="hire">
                    <h1><em>Hello!</em> I'm a freelance web designer that likes to make amazing websites.</h1>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="services" data-stellar-background-ratio="0.5">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <h1>I strongly believe that web design should be:</h1>
            </div>
        </div>
        <p class="divider">&nbsp</p>
        <div class="row">
            <div class="col-md-3 col-sm-3 col-xs-6">
                <p class="glyphicon glyphicon-pencil"></p>
                <h2>Simple</h2>
                <p>Transparent, uncomplicated design</p>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-6">
                <p class="glyphicon glyphicon-tasks"></p>
                <h2>Elegant</h2>
                <p>Graceful layouts & colour palettes</p>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-6">
                <p class="glyphicon glyphicon-user"></p>
                <h2>User Friendly</h2>
                <p>Making it easier for people to use</p>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-6">
                <p class="glyphicon glyphicon-ok"></p>
                <h2>W3C Compliant</h2>
                <p>Strict standardised coding</p>
            </div>
        </div>
    </div>
</section>
<section id="about">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-4">
                <img src="<?php echo $base_url; ?>/img/liam_left.png" class="img-responsive" />
            </div>
            <div class="col-md-8 col-sm-8">
                <h1>About Liam (<?php echo $age . ' years old'; ?>)</h1>
                <p>For the last 5 years, Liam has been working as a full time web designer. He lives in Nuneaton, in the UK.</p>
                <p>With no 'traditional' training, many of his skills have been gained through self motivation and general practice. This has enabled him to be flexible in what skills he has developed and what techniques he likes to apply.</p>
                <p>As he works as a full time web designer, he is always looking for new and exciting opportunities to create amazing websites that can inspire others.</p>
                <p>Plus...he has a kick ass beard.</p>
            </div>
        </div>
    </div>
</section>
<section id="quote">
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-sm-9">
                <blockquote>
                    "An <em>exciting</em>, high energy individual that thrives on challenging projects"</br>
                    <cite>In One Sentence, How I Would Describe Myself...</cite>
                </blockquote>
            </div>
            <div class="col-md-3 col-sm-3">
                <a href="#" class="btn btn-default btn-lg" data-toggle="modal" data-target="#myModal">Hire Me</a>
            </div>
        </div>
    </div>
</section>
<section id="brands">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>My brain understands these things...</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2 col-sm-2 col-xs-6">
                <a href="http://www.magento.com/" target="_blank" rel="nofollow" title="Magento"><img src="<?php echo $base_url; ?>/img/magento.png" alt="Magento" class="img-responsive"/></a>
            </div>
            <div class="col-md-2 col-sm-2 col-xs-6">
                <a href="https://www.wordpress.com/" target="_blank" rel="nofollow" title="Wordpress"><img src="<?php echo $base_url; ?>/img/wordpress.png" alt="Wordpress" class="img-responsive"/></a>
            </div>
            <div class="col-md-2 col-sm-2 col-xs-6">
                <a href="http://www.w3.org/" target="_blank" rel="nofollow" title="HTML5, CSS3 & Responsive Web Design"><img src="<?php echo $base_url; ?>/img/html5.png" alt="HTML5, CSS3 & Responsive Web Design" class="img-responsive"/>
            </div>
            <div class="col-md-2 col-sm-2 col-xs-6">
                <a href="https://www.adobe.com/uk/products/photoshop.html?promoid=KLXLS" target="_blank" rel="nofollow" title="Adobe Creative Cloud - Photoshop & Illustrator"><img src="<?php echo $base_url; ?>/img/adobe.png" alt="Adobe Creative Cloud" class="img-responsive"/></a>
            </div>
            <div class="col-md-2 col-sm-2 col-xs-6">
                <a href="https://www.mysql.com/" target="_blank" rel="nofollow" title="MySQL Database"><img src="<?php echo $base_url; ?>/img/mysql.png" alt="MySQL DB" class="img-responsive"/></a>
            </div>
            <div class="col-md-2 col-sm-2 col-xs-6">
                <a href="https://git-scm.com/" target="_blank" rel="nofollow" title="Git - Version Control"><img src="<?php echo $base_url; ?>/img/git.png" alt="Git - Version Control" class="img-responsive"/></a>
            </div>
        </div>
    </div>
</section>
<?php require_once('popup.php'); ?>
<?php require_once('./layouts/footer.php'); ?>
