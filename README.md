# jabberdog

What is it?
- - - - - - -

Liam Fathers (aka Jabberdog Web Design) is a freelance web designer. Liam is always looking for new and exciting opportunities to apply his skills in web design.
Liam is excited by new projects which focus on clean clear design, elegant layouts, user friendly functions and w3c compliant coding.


Website Version
- - - - - - -

The current version of this site can be found online at http://www.jabberdog.co.uk/.


Contacts
- - - - - - -

If you want to contact Jabberdog with any questions or job opportunities then please feel free to email him at <liam@jabberdog.co.uk>.